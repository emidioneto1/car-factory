package carfactory

import (
	"encoding/json"
	"fmt"
)

type Car struct {
	Model        Model
	Year         int
	EngineSerial string
}

type Model struct {
	Brand   string
	Name    string
	Version int
}

type Defect struct {
	CarModel      Model
	AffectedYears []int
	Code          string
}

func (m Model) UserFriendly() string {
	return fmt.Sprintf("%s %s v%d", m.Brand, m.Name, m.Version)
}

func (m Model) DeveloperFriendly() ([]byte, error) {
	c := struct {
		Brand   string `json:"brand"`
		Name    string `json:"name"`
		Version int    `json:"version"`
	}{
		Brand:   m.Brand,
		Name:    m.Name,
		Version: m.Version,
	}

	return json.Marshal(c)
}

func (m Model) IncrementVersion() Model {
	m.Version++

	return m
}
