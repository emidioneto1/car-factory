package carfactory

type DefectService interface {
	FindKnownDefects([]Car) ([]Defect, error)
}

type defectServiceClient struct {
	knownDefects []Defect
}

type DefectServiceOption func(*defectServiceClient)

func (df Defect) Affects(car Car) bool {
	if df.CarModel.Brand != car.Model.Brand || df.CarModel.Name != car.Model.Name || df.CarModel.Version != car.Model.Version {
		return false
	}

	for _, ay := range df.AffectedYears {
		if ay == car.Year {
			return true
		}
	}
	return false
}

func NewDefectService(opts ...DefectServiceOption) defectServiceClient {
	ds := defectServiceClient{
		knownDefects: []Defect{},
	}

	for _, opt := range opts {
		opt(&ds)
	}

	return ds
}

func WithKnownDefects(knownDefects []Defect) DefectServiceOption {
	return func(ds *defectServiceClient) {
		ds.knownDefects = knownDefects
	}
}

func (ds defectServiceClient) DetectDefects(cars []Car) []Defect {
	var found []Defect

	for _, car := range cars {
		if df := ds.findDefects(car); df != nil {
			found = append(found, df...)
		}
	}

	return found
}

func (ds defectServiceClient) findDefects(car Car) []Defect {
	var found []Defect

	for _, defect := range ds.knownDefects {
		if defect.Affects(car) {
			found = append(found, defect)
		}
	}

	return found
}
