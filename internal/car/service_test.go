package carfactory

import (
	"reflect"
	"testing"
)

var (
	firstCar = Car{
		Model: Model{
			Brand:   "VW",
			Name:    "Golf",
			Version: 5,
		},
		Year:         2003,
		EngineSerial: "11A-12345",
	}
	secondCar = Car{
		Model: Model{
			Brand:   "VW",
			Name:    "Golf",
			Version: 5,
		},
		Year:         2008,
		EngineSerial: "88Z-88888",
	}
	knownDefects = []Defect{
		{
			CarModel: Model{
				Brand:   "VW",
				Name:    "Golf",
				Version: 5,
			},
			AffectedYears: []int{2003},
			Code:          "345F",
		},
	}
)

func TestDefect_Affects(t *testing.T) {
	type fields struct {
		Model         Model
		AffectedYears []int
		Code          string
	}
	type args struct {
		car Car
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   bool
	}{
		{
			name: "no_defect_for_given_car_model",
			fields: fields{
				Model: Model{
					Brand:   "VW",
					Name:    "Golf",
					Version: 5,
				},
				AffectedYears: []int{2001},
			},
			args: args{
				Car{
					Model: Model{
						Brand:   "VW",
						Name:    "Golf",
						Version: 6,
					},
					Year: 2020,
				},
			},
			want: false,
		},
		{
			name: "no_defect_for_given_year",
			fields: fields{
				Model: Model{
					Brand:   "VW",
					Name:    "Golf",
					Version: 5,
				},
				AffectedYears: []int{2001},
			},
			args: args{
				Car{
					Model: Model{
						Brand:   "VW",
						Name:    "Golf",
						Version: 5,
					},
					Year: 2005,
				},
			},
			want: false,
		},
		{
			name: "has_defect",
			fields: fields{
				Model: Model{
					Brand:   "VW",
					Name:    "Golf",
					Version: 5,
				},
				AffectedYears: []int{2001},
			},
			args: args{
				Car{
					Model: Model{
						Brand:   "VW",
						Name:    "Golf",
						Version: 5,
					},
					Year: 2001,
				},
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			df := Defect{
				CarModel:      tt.fields.Model,
				AffectedYears: tt.fields.AffectedYears,
				Code:          tt.fields.Code,
			}
			if got := df.Affects(tt.args.car); got != tt.want {
				t.Errorf("Defect.Affects() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_DetectDefects(t *testing.T) {
	tests := []struct {
		name         string
		knownDefects []Defect
		cars         []Car
		want         []Defect
	}{
		{
			name:         "should_find_defects",
			knownDefects: knownDefects,
			cars:         []Car{firstCar, secondCar},
			want:         knownDefects,
		},
		{
			name:         "should_not_find_defects",
			knownDefects: knownDefects,
			cars:         []Car{secondCar},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ds := NewDefectService(WithKnownDefects(tt.knownDefects))
			if got := ds.DetectDefects(tt.cars); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DetectDefects() = %v, want %v", got, tt.want)
			}
		})
	}
}
