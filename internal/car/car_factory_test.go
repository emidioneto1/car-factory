package carfactory

import (
	"reflect"
	"testing"
)

func TestCarModel_UserFriendly(t *testing.T) {
	type fields struct {
		Brand   string
		Name    string
		Version int
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "user_friendly",
			fields: fields{
				Brand:   "VW",
				Name:    "Golf",
				Version: 5,
			},
			want: `VW Golf v5`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cm := Model{
				Brand:   tt.fields.Brand,
				Name:    tt.fields.Name,
				Version: tt.fields.Version,
			}
			if got := cm.UserFriendly(); got != tt.want {
				t.Errorf("CarModel.UserFriendly() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCarModel_DeveloperFriendly(t *testing.T) {
	type fields struct {
		Brand   string
		Name    string
		Version int
	}
	tests := []struct {
		name    string
		fields  fields
		want    []byte
		wantErr bool
	}{
		{
			name: "developer_friendly",
			fields: fields{
				Brand:   "VW",
				Name:    "Golf",
				Version: 1,
			},
			want: []byte("{\"brand\":\"VW\",\"name\":\"Golf\",\"version\":1}"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cm := Model{
				Brand:   tt.fields.Brand,
				Name:    tt.fields.Name,
				Version: tt.fields.Version,
			}
			got, err := cm.DeveloperFriendly()
			if (err != nil) != tt.wantErr {
				t.Errorf("CarModel.DeveloperFriendly() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CarModel.DeveloperFriendly() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCarModel_IncrementVersion(t *testing.T) {
	type fields struct {
		Brand   string
		Name    string
		Version int
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{
			name: "increment_car_version",
			fields: fields{
				Brand:   "VW",
				Name:    "Golf",
				Version: 1,
			},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cm := Model{
				Brand:   tt.fields.Brand,
				Name:    tt.fields.Name,
				Version: tt.fields.Version,
			}
			if got := cm.IncrementVersion(); got.Version != tt.want {
				t.Errorf("CarModel.IncrementVersion() = %v, want %v", got, tt.want)
			}
		})
	}
}
